const { Client, RichEmbed } = require('discord.js');
const client = new Client();
const auth = require('./auth.json');
const package = require('./package.json');
const _f = require('./functions.js');
var args;
var cmd;

// User Variables:
prefix = '!';
adminPrefix = '+';

function kickUnverified() {
	try {
		if (_f.exVerified === 0) {
			console.log(`[${_f.timestamp()}][V] User did not verify and has been kicked.`);
			newUser.send("You have been kicked from Texas ABDLs because you failed to read the rules and verify that you weren't a bot.");
			newUser.kick("Did not verify.");
			newUser = "";
			_f.exVerified = 0;
		} else if (_f.exVerified === 1) {
			console.log(`[${_f.timestamp()}][V] User verified and will not be kicked.`);
			newUser = "";
			_f.exVerified = 0;
		}
	}
	catch(err) {
		console.error(`[${_f.timestamp()}][E] ${err}`);
	}
}

client.on('ready', () => {
	console.log(`[${_f.timestamp()}][I] Stuffy bot online.`);
	// console.log(`[V]: Verbose\n[I]: Info\n[E]: Error`);
	var channel = client.channels.get('513915468235407361');
	// if (!channel) return;
	// channel.send("Good morning! I'm awake.");
});

client.on('message', message => {
	try {
		var firstChar = message.content.slice(0,1);
		switch (firstChar) {
			case prefix:
				args = message.content.slice(1).trim().split(/ +/g);
				cmd = args.shift().toLowerCase();
				console.log(`[${_f.timestamp()}]${_f.botCmd(cmd, args, message)}`);
				break;
			case adminPrefix:
				args = message.content.slice(1).trim().split(/ +/g);
				cmd = args.shift().toLowerCase();
				console.log(`[${_f.timestamp()}]${_f.botCmdAdmin(cmd, args, message)}`);
				break;
		}
	}
	catch (err) {
		console.error(`[${_f.timestamp()}][E] ${err}`);
	}

	try {
		if (typeof newUser !== 'undefined' && typeof message.member.id !== 'null') {
			if (message.member.id === newUser.id) {
				if (message.content.toLowerCase() === 'abdl') {
					console.log(`[${_f.timestamp()}][V] User verified.`);
					message.channel.send(newUser.user + ' is verified!');
					_f.exVerified = 1;
				}	
			}
		}
	}
	catch (err) {
		console.error(`[${_f.timestamp()}][E] ${err}`);
	}
});

client.on('guildMemberAdd', member => {
	console.log(`[${_f.timestamp()}][V] New member joined.`);
	newUser = member;
	var channel = member.guild.channels.find(ch => ch.name === 'playground');
	if (!channel) return;
	channel.send(_f.exRules);
	console.log(`[${_f.timestamp()}][V] Sent rules to new member.`);

	if (_f.exFilter === 1) {
		setTimeout(kickUnverified, 300000);
	}
});

client.login(auth.token);