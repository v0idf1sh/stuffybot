const { Client, RichEmbed } = require('discord.js');
const Datastore = require('nedb');
const cron = require('node-cron');
const client = new Client();
const auth = require('/home/abby/stuffybot/auth.json');
const package = require('/home/abby/stuffybot/package.json');
const _f = require('/home/abby/stuffybot/functions.js');
const prefs = new Datastore({ filename: '/home/abby/stuffybot/prefs.db', autoload: true });
var args;
var cmd;

// User Variables:
magicNumber = 42;
prefix = '!';
adminPrefix = '+';

client.on('ready', () => {
	var logChannel = client.channels.get('521713511508738068');
	logChannel.send(`[I] Stuffy bot online.`);

	var guilds = client.guilds.array();
	
	for(i = 0; i < guilds.length; i++) {
		x = i;
		prefs.findOne({_id: guilds[x].id}, function(err, doc) {
			if (!doc) {
				defaultPrefs = {_id: guilds[x].id, filter: 0};
				prefs.insert({_id: guilds[x].id, filter: 0}, function(err, doc) {
					console.log("Created prefs.");
				});
			}
		});
	}
});

cron.schedule('0 * * * *', () => {
	_f.exOptIn.find({}, function(err, docs) {
		if (docs) {
			for (i = 0; i < docs.length; i++) {
				date = new Date();
				e = date.getHours();
				c = date.getHours() - 1;
				m = date.getHours() - 2;
				p = date.getHours() - 3;
				timeString = `Here's your hourly alert!\n\`E: ${e}:00\`\n\`C: ${c}:00\`\n\`M: ${m}:00\`\n\`P: ${p}:00\`\n*=*=*=*=*=*`;
				client.users.get(`${docs[i]._id}`).send(timeString);
			}
		}
	});
});

function kickUnverified() {
	var logChannel = client.channels.get('521713511508738068');
	try {
		if (_f.exVerified === 0) {
			logChannel.send(`[V] User did not verify and has been kicked.`);
			newUser.send("You have been kicked from the server because you failed to read the rules and verify that you weren't a bot.");
			newUser.kick("Did not verify.");
			newUser = "";
			_f.exVerified = 0;
		} else if (_f.exVerified === 1) {
			logChannel.send(`[V] User verified and will not be kicked.`);
			newUser = "";
			_f.exVerified = 0;
		}
	}
	catch(err) {
		console.error(`[E] ${err}`);
	}
}

const getDefaultChannel = (guild) => {
  	// get "original" default channel
  	if(guild.channels.has(guild.id))
    	return guild.channels.get(guild.id)

	  // Check for a "general" channel, which is often default chat
  	const generalChannel = guild.channels.find(channel => channel.name === "general");
  	if (generalChannel)
	    return generalChannel;
  	// Now we get into the heavy stuff: first channel in order where the bot can speak
  	// hold on to your hats!
  	return guild.channels
   		.filter(c => c.type === "text" &&
     		c.permissionsFor(guild.client.user).has("SEND_MESSAGES"))
   		.sort((a, b) => a.position - b.position ||
     		Long.fromString(a.id).sub(Long.fromString(b.id)).toNumber())
   		.first();
}

client.on('message', message => {
	var logChannel = client.channels.get('521713511508738068');
	
	//TODO - Make an opt-in for the random messages
	// randNum = Math.floor(Math.random() * 50);
	// if (message.author.id != '513184762073055252') {
	// 	if (randNum === magicNumber) {
	// 		message.channel.send(`I'm sorry ${message.author}, I can't do that.`);
	// 	}
	// }
	try {
		var firstChar = message.content.slice(0,1);
		switch (firstChar) {
			case prefix:
				args = message.content.slice(1).trim().split(/ +/g);
				cmd = args.shift().toLowerCase();
				logChannel.send(`${_f.botCmd(cmd, args, message)}`);
				break;
			case adminPrefix:
				args = message.content.slice(1).trim().split(/ +/g);
				cmd = args.shift().toLowerCase();
				logChannel.send(`${_f.botCmdAdmin(cmd, args, message)}`);
				break;
		}
	}
	catch (err) {
		console.error(`[E] ${err}`);
	}

	try {
		if (typeof newUser !== 'undefined' && typeof message.member.id !== 'null') {
			if (message.member.id === newUser.id) {
				if (message.content.toLowerCase() == 'not a bot') {
					logChannel.send(`[V] User verified.`);
					newUser.addRole('517919614777425921');
					newUser.removeRole('530332853439823872');
					message.channel.send(newUser.user + ' is verified!');
					_f.exVerified = 1;
				}	
			}
		}
	}
	catch (err) {
		console.error(`[E] ${err}`);
	}
});

client.on('guildMemberAdd', (member) => {
	rules = new Datastore({ filename: '/home/abby/stuffybot/rules.db', autoload:true })
	var groupRules = "Rules";
	var logChannel = client.channels.get('521713511508738068');
	logChannel.send(`[V] New member joined.`);
	newUser = member;
	const channel = client.channels.get('530334007213293571');
	member.addRole('530332853439823872');

	rules.findOne({_id: `${member.guild.id}`}, function(err, doc) {
		if (doc) {
			channel.send(doc.text);
		} else {
			channel.send(`There was an error getting the rules for your channel: ${member.guild.id}`);
		}
	})

	//channel.send(groupRules);
	logChannel.send(`[V] Sent rules to new member.`);

	if (_f.exFilter === 1) {
		setTimeout(kickUnverified, 300000);
	}
});

client.login(auth.token);
