#// Dependencies

Here is the entire list of programs necessary for fresh install.

:: `nodejs`  
:: node modules:  
::::`nedb`  
::::`node-cmd`  
::::`discord.js`  
::::`giphy-api`  
::::`node-cron`

#// Command List
// Games  
`!8ball` - 8 Ball Fortune Teller.  
`!roll x y` - Roll `x` number of `y`-sided dice.  
`!random [x] y` - Get a random number between `x` and `y`, `x` is optional, if only `y` is given you will get a number between 0 and `y`.  

// Group Management  
`!rules` - Retreive the group's rules.

// Fun Stuff  
`!diapercheck` - Roll Call! Who's wet? xD  
`!gif <search>` - Search Giphy for a GIF.  
`!sticker <search>` - Search Giphy for a Sticker.  
`!snack` - Gives everyone a snack, what'll it be??  
`!hug` - Gives you a hug. //TODO: Allow giving others hugs.

// Profile  
== Profiles are global and public, they can be seen by anyone you share a server with.  
`!profile new <profile>` - Creates a new profile for you, this can only be used if you don't already have a profile.  
`!profile edit <profile>` - Edits your current profile.  
`!profile delete` - Deletes your profile, **THIS IS IRREVERSIBLE**  
`!profile view` - Shows your current profile.  
`!examine @someone` - Shows `@someone`'s profile.

// Bot Meta  
`!help` - Retreives the bot's help text.  
`!suggest <suggestion>` - Submit a suggestion to me for the bot.  
`!ping` - Pings the bot to check if it's online.  
`!userinfo` - Shows some info about you, to help get IDs without enabling dev mode.

// Admin  
`+setrules <rules>` - Set the channel's rules.

#// Coming Soon
`!motd` - Message of the day, per server. Currently only implemented globally.  
`!optin` - Opt in to hourly messages. May never be implemented as too many people would cause issues with Discord's API.  
`!optout` - Opt out of hourly messages.  
`+filter/[on]/[off]` - Per server bot/troll filter.