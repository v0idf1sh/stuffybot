const { Client, RichEmbed } = require('discord.js');
const package = require('/home/abby/stuffybot/package.json');
var Datastore = require('nedb');
var profiles = new Datastore({ filename: '/home/abby/stuffybot/profiles.db', autoload: true });
var suggestions = new Datastore({ filename: '/home/abby/stuffybot/suggestions.db', autoload: true });
var rules = new Datastore({ filename: '/home/abby/stuffybot/rules.db', autoload: true });
var optedIn = new Datastore({ filename: './optedIn.db', autoload: true });
var help = "// Command List\n\n// Games\n\n`!8ball` - 8 Ball Fortune Teller.\n`!roll x y` - Roll `x` number of `y`-sided dice.\n`!random [x] y` - Get a random number between `x` and `y`, `x` is optional, if only `y` is given you will get a number between 0 and `y`.\n\n// Group Management\n`!rules` - Retreive the group's rules.\n\n// Fun Stuff\n`!diapercheck` - Roll Call! Who's wet? xD\n`!gif <search>` - Search Giphy for a GIF.\n`!sticker <search>` - Search Giphy for a Sticker.\n`!snack` - Gives everyone a snack, what'll it be??\n`!hug` - Gives you a hug. //TODO: Allow giving others hugs.\n\n// Profile\n== Profiles are global and public, they can be seen by anyone you share a server with.\n`!profile new <profile>` - Creates a new profile for you, this can only be used if you don't already have a profile.\n`!profile edit <profile>` - Edits your current profile.\n`!profile delete` - Deletes your profile, **THIS IS IRREVERSIBLE**\n`!profile view` - Shows your current profile.\n`!examine @someone` - Shows `@someone`'s profile.\n\n// Bot Meta\n`!help` - Retreives the bot's help text.\n`!suggest <suggestion>` - Submit a suggestion to me for the bot.\n`!ping` - Pings the bot to check if it's online.\n`!userinfo` - Shows some info about you, to help get IDs without enabling dev mode.\n\n// Admin\n`+setrules <rules>` - Set the channel's rules.\n\nStuffy Bot maintained by @LittleBrattyAbby#3410 - Version: " + package.version;
// var rules = "Texas ABDLs Group Rules\n\nThis group is strictly 18+\nUpon joining the group please message an admin with a face pic and the meaning of ABDL.\n\n1. Do not PM anyone without permission.\n2. No nudity\n3. Be a decent and respectful person.\n4. No bigotry or -phobias.\n\nViolators will be removed or banned depending on severity.\n\nType \"!help\" to see a list of my commands.\nPlease type \"ABDL\" to verify you're not a bot, otherwise you will automatically be kicked.";
var snacks = ['cookies and milk','crackers and cheese','candy','oreos and milk','cheese sticks','pop tarts'];
var motd = "No MOTD Set...";
var verified = 0;
var filter = 0;

function timestamp() {
	date = new Date;
	dateString = date.toISOString();
	return dateString;
}

var cmd = require('node-cmd');

module.exports = {
	exRules: rules,
	exVerified: verified,
	exFilter: filter,
	exOptIn: optedIn,
	timestamp: function () {
		date = new Date();
		dateString = date.toISOString();
		return dateString;
	},
	botCmd: function (command, arguments, message) {
		switch (command) {
			case 'motd':
				message.channel.send(motd);
				break;
			case 'optin':
				optedIn.findOne({_id: `${message.member.id}`}, function(err, doc) {
					if (doc) {
						message.channel.send(`You've already opted in!`);
					} else {
						newdoc = {_id: `${message.member.id}`};
						optedIn.insert(newdoc, function(err, doc) {
							message.channel.send(`You've opted in!`);
						})
					}
				});
				break;
			case 'optout':
				optedIn.findOne({_id: `${message.member.id}`}, function(err, doc) {
					if (doc) {
						optedIn.remove({_id: `${message.member.id}`}, function(err, numRemoved) {
							if (numRemoved === 1) {
								message.channel.send(`Successfully opted out.`);
							} else {
								message.channel.send(`There was an error opting out.`);
							}
						})
					} else {
						message.channel.send(`You've already opted out.`);
					}
				});
				break;
			case '8ball':
				var results = ['It is certain.', 'It is decidedly so.', 'Without a doubt.', 'Yes - definitely.', 'You may rely on it.', 'As I see it, yes.', 'Most likely.', 'Outlook good.', 'Yes.', 'Signs point to yes.', 'Reply hazy, try again.', 'Ask again later.', 'Better not tell you now.', 'Cannot predict now.', 'Concentrate and ask again.', 'Don\'t count on it.', 'My reply is no.', 'My sources say no.', 'Outlook not so good.', 'Very doubtful.'];
				var rand = Math.floor(Math.random() * results.length);
				message.channel.send(results[rand]);
				return "[V] 8-Ball";
				break;
			case 'diapercheck':
				message.channel.send("Diaper Check! *checks everyones diapers* (Feel free to send pics!)");
				break;
			case 'roll':
				// Rolls a dice. Very similar to !random, but allows multiple draws at once.
				// Syntax: !roll <number of dice> <sides on dice>
				// Example: !roll 2 6 (Roll 2 6-sided dice aka 2d6)
				switch (arguments.length) {
					case 0:
						message.channel.send("Roll expects 2 arguments, How many dice and how many sides the dice have.");
						break;
					case 1:
						message.channel.send("Roll expects 2 arguments, How many dice and how many sides the dice have.");
						break;
					case 2:
						totalRoll = 0;
						rollString = "";
						numberOfDice = parseInt(arguments[0]);
						diceSides = parseInt(arguments[1]) + 1; // Adds one to account for Math.random's lack of inclusiveness
						if (numberOfDice <= 5) {
							for (i = 0; i < numberOfDice; i++) {
								diceRoll = Math.floor(Math.random() * diceSides);
								rollString = `${rollString}\nDice #${i + 1}: ${diceRoll}`;
								totalRoll = totalRoll + diceRoll;
							}
							message.channel.send(rollString);
							message.channel.send(`Total: ${totalRoll}`);
						} else {
							for (i = 0; i < numberOfDice; i++) {
								diceRoll = Math.floor(Math.random() * diceSides);
								totalRoll = totalRoll + diceRoll;
							}
							avgRoll = totalRoll / numberOfDice;
							message.channel.send(`Total: ${totalRoll}\nAverage: ${avgRoll}`);
						}
						break;
				}
				return '[V] Rolled Dice.';
				break;
			case 'random':
				switch (arguments.length) {
					case 0:
						message.channel.send('You must specify a maximum number.');
						break;
					case 1:
						randMax = arguments[0];
						rand = Math.floor(Math.random() * randMax);
						message.channel.send(rand);
						break;
					case 2:
						min = parseInt(arguments[0]);
						max = parseInt(arguments[1]);
						a = max - min + 1;
						b = Math.floor(Math.random() * a);
						c = b + min;
						message.channel.send(c);
						break;
				}
				return "[V] Generated random number.";
				break;
			case 'suggest':
				tmpArgs = arguments.join(' ');
				tmpSuggestion = { suggestion: `${tmpArgs}` };
				suggestions.insert(tmpSuggestion, function(err, doc) {
					message.channel.send('Suggestion submitted.');
				});
				return '[V] Suggestion submitted.';
				break;
			case 'probe':
				message.channel.send(`Probes people randomly.`);
				return "[V] Probed.";
				break;
			case 'gif':
				var giphy = require('giphy-api')('B1SX3md8dP9ClBZj0rZo06Bh5zQ8JdQg');
				var searchString = arguments.join(' ');
				try {
					giphy.search(searchString, function (err, res) {
						if (res.data.length >= 5) {
							var rand = Math.floor(Math.random() * 5);
							if (res.data[rand]) {	
								var gifURL = res.data[rand].url;
								var gifEmbedURL = res.data[rand].images.original.url;
								var gifTitle = res.data[rand].title;
			
								const embed = new RichEmbed();
								embed.file = gifEmbedURL;
								embed.title = gifTitle;
			
								message.channel.send(embed);
							} else {
								message.channel.send("No GIF found for "+searchString+".");
							}
						} else {
							var rand = Math.floor(Math.random() * res.data.length);
							if (res.data[rand]) {	
								var gifURL = res.data[rand].url;
								var gifEmbedURL = res.data[rand].images.original.url;
								var gifTitle = res.data[rand].title;
			
								const embed = new RichEmbed();
								embed.file = gifEmbedURL;
								embed.title = gifTitle;
			
								message.channel.send(embed);
							} else {
								message.channel.send("No GIF found for "+searchString+".");
							}
						}
					});
				}
				catch (err) {
					message.channel.send("No GIF found for ",searchString,".");
				}
				return "[V] GIF Requested.";
				break;
			case 'sticker':
				var giphy = require('giphy-api')('B1SX3md8dP9ClBZj0rZo06Bh5zQ8JdQg');
				var searchString = arguments.join(' ');
				try {
					giphy.search({
						api: 'stickers',
						q: searchString
					}, function (err, res) {
						var rand = Math.floor(Math.random() * 5);
						if (res.data[rand]) {
							var stickerURL = res.data[rand].url;
							var stickerEmbedURL = res.data[rand].images.original.url;
							var stickerTitle = res.data[rand].title;
		
							const embed = new RichEmbed();
							embed.file = stickerEmbedURL;
							embed.title = stickerTitle;
		
							message.channel.send(embed);
						} else {
							message.channel.send("No sticker found for "+searchString+".");
						}
					});
				}
				catch (err) {
					message.channel.send("No sticker found for ",searchString,".");
				}
				break;
			case 'help':
				message.channel.send(help);
				return "[V] Help Page Requested.";
				break;
			case 'ping':
				message.channel.send(`Pong!`);
				return "[V] Pinged.";
				break;
			case 'rules':
				rules.findOne({_id: `${message.guild.id}`}, function(err, doc) {
					if (doc) {
						message.channel.send(doc.text);
					} else {
						message.channel.send(`Sorry, I was unable to find the rules for your group.`);
					}
				});
				return "[V] Rules Requested.";
				break;
			case 'setrules':
				tmpArgs = arguments;
				rules.findOne({_id: `${message.guild.id}`}, function(err, doc) {
					
					newRules = tmpArgs.join(' ');
					if (doc) {
						rules.update({_id: `${doc._id}`}, {$set: {text: `${newRules}`}}, function(err, numReplaced) {
							if (numReplaced === 1) {
								message.channel.send(`Rules updated!`);
							}
						});
					} else {
						var obj = {_id: `${message.guild.id}`, text: `${newRules}`};
						rules.insert(obj, function(err, doc) {
							message.channel.send(`Rules updated!`);
						});
					}
				});
				break;
			case 'snack':
				var randSnack = Math.floor(Math.random() * snacks.length);
				message.channel.send('Snack time! *heads to the kitchen and comes back, handing out ' + snacks[randSnack] + ' to everyone.*');
				return "[V] Handed out snacks.";
				break;
			case 'hug':
				message.channel.send(`*gives ${message.author} a hug*`);
				return "[V] Hug Given.";
				break;
			case 'profile':
				var outputText = "Default Text";
				switch (arguments[0]) {
					case 'new':
						// Create new profile unique to `message.author`
						var tmpArgs = arguments;
						var tmpMsg = message;
						profiles.findOne({_id: `${message.author.id}`}, function(err, doc) {
							if (doc) {
								console.log(`    : Cannot set new profile. One already exists.`);
								tmpMsg.channel.send('Cannot set new profile. One already exists. Please use !profile edit {new profile}');								
							} else {
								var profileText = tmpArgs.slice(1).join(' ');
								var profile = {_id: `${message.author.id}`, profile: `${profileText}`};
								profiles.insert(profile, function(err, doc) {
									tmpMsg.channel.send(`New Profile: <@${doc._id}>`);
								});
							}
						})
						return "[V] New profile.";
						break;
					case 'delete':
						var tmpMsg = message;
						profiles.findOne({_id: `${message.member.id}`}, function(err, doc) {
							if (doc) {
								profiles.remove({_id: `${message.member.id}`}, function(err, numDeleted) {
									message.channel.send(`Profile deleted.`);
								})
							} else {
								message.channel.send(`There is no profile to delete.`);
							}
						});
						return "[V] Deleted profile.";
						break;
					case 'edit':
						// Edit the profile associated with `message.author`
						var tmpArgs = arguments;
						profileText = tmpArgs.slice(1).join(' ');
						var tmpMsg = message;
						profiles.findOne({_id: `${message.member.id}`}, function(err, doc) {
							if (doc) {
								profiles.update({_id: `${doc._id}`}, {$set: {profile: `${profileText}`}}, function(err, numReplaced) {
									message.channel.send(`Profile Updated.`);
								});
							} else {
								message.channel.send(`Error updating the profile.`);
							}
						});
						return "[V] Edited profile.";
						break;
					case 'view':
						// View message.author's profile
						profiles.findOne({_id: `${message.member.id}`}, function(err, doc) {
							if (doc) {
								message.channel.send(`<@${doc._id}>\'s Profile: ${doc.profile}`);
							} else {
								message.channel.send(`I was unable to find your profile.`);
							}
						});
						return "[V] Got self profile.";
						break;
				}
				return "[V] Profile::";
				break;
			case 'examine':
				// Get the profile of a mentioned user and send it.
				var mentionArray = message.mentions.users.array();
				var mentioned = `${mentionArray[0].id}`;
				profiles.findOne({_id: `${mentioned}`}, function(err, doc) {
					if (doc) {
						message.channel.send(`<@${doc._id}>\'s Profile: ${doc.profile}`);
					} else {
						message.channel.send(`<@${mentioned}> does not have a profile.`);
					}
				});
				return "[V] Got profile";
				break;
			case 'userinfo':
				var userInfo = `Your ID: ${message.author.id}\nYour highestRole ID: ${message.member.highestRole.id}`;
				message.channel.send(userInfo);
				break;
		}
	},
	botCmdAdmin: function (command, arguments, message) {
		switch (command) {
			case 'motd':
				motd = arguments.join(' ');
				message.channel.send(motd);
				break;
			case 'getsuggestions':
				suggestions.find({}, function (err, docs) {
					tmpString = "";
					for (i = 0; i < docs.length; i++) {
						tmpString = tmpString + docs[i].suggestion + "\n\n";
					}
					message.channel.send(tmpString);
				});
				break;
			case 'deletesuggestions':
				suggestions.remove({}, {multi: true}, function(err, numRemoved) {
					message.channel.send("Deleted " + numRemoved + " entries.");
				});
				break;
			case 'reboot':
				cmd.get('sudo /home/abby/bots/stuffybot/reboot', function(err, data, stderr) {
					
				});
				return "[V] Going down for reboot now.";
				break;
			case 'filter':
				message.channel.send(`Filter Status: ${filter}`);
				return "[V] Sent Filter Status";
				break;
			case 'filteron':
				filter = 1;
				message.channel.send('Bot filter enabled.');
				return "[V] Enabled Bot Filter";
				break;
			case 'filteroff':
				filter = 0;
				message.channel.send('Bot filter diabled.');
				return "[V] Disabled Bot Filter";
				break;
		}
	}
}